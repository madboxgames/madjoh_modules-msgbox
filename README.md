# msgbox #

v1.1.1

** This documentation is out dated ! **

This module enables displaying custom alerts.

## Getting Started ##

### Define the msgbox in your HTML ###

You need to have this kind of block in your html file.

```html
<div id="msgbox">
	<span id="msgbox-close">x</span>
	<h1 id="msgbox-title"></h1>
	<p id="msgbox-message"></p>
	<span id="msgbox-ok" class="Button">OK</span>
</div>
```

HTML tags are arbitrary.
id attributes are mandatory.

### Define the settings ###
```js
// Edit the file at : app/script/settings/alert.js

showMsgBox : function(){
	// SHOW ALERT
},
hideMsgBox : function(){
	// HIDE ALERT
}
```

You can use transitions such as SlideTransition, FlashTransition or any other to show and hide the msgbox.

### Fire the alert with JavaScript ###

```js
CustomAlert(title, message, [callback, [ok]]);

// OR

CustomAlert(parameters);
```

| Parameter 		| Type 		| Default 	| Description 													|
| ------------------| ----------|-----------| --------------------------------------------------------------|
| title*			| string 	| 			| The alert title												|
| message* 			| string 	|			| The alert message 											|
| callback 			| function 	| none		| The function to execute if the user presses the ok button. 	|
| ok 				| string 	| "OK"		| The label to print in the ok button 							|
| parameters* 		| object 	| 			| A JSON object defining the parameters of the function 		|

** Example **
```js
// Version 1
CustomAlert(
	'Congratulations',
	'This is your first custom alert !' ,
	function(){
		alert('You don\'t need these alerts anymore !');
	}
);

// Version 2 (available since v1.0.2)
var parameters = {
	text : {
		title 	: 'Hello World !',
		message : 'What a beautiful day, isn\'t it ?',
		ok 		: 'Indeed'
	},
	onOk : function(){
		// Executed when the user clicks on the ok button.
		console.log('The user agrees !');
	},
	onClose : function(){
		// Executed when the user clicks on the close button.
		console.log('The user disagrees !');
	}
};

CustomAlert(parameters);
```