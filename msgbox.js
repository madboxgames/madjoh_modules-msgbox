define([
	'require',
	'madjoh_modules/wording/wording'
],
function(require, Wording){
	var MsgBox = {
		init : function(settings){
			MsgBox.settings = settings;
		},
		show : function(messageKey, params){
			if(!messageKey) return console.log('Missing message key !');

			if(!params) params = {};
			if(!params.onClose) 	params.onClose 	= function(){};
			if(!params.onOk) 		params.onOk 	= function(){};

			var textKeys = Wording.messages[messageKey];
			if(!textKeys) return console.log('Invalid Message Key', messageKey);
			if(!textKeys.ok) textKeys.ok = 'ok';

			params.text = {};
			for(var key in textKeys) params.text[key] = Wording.getText(textKeys[key]);
			
			MsgBox.settings.show(params);
		}
	};

	return MsgBox;
});